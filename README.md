# https://github.com/docker-library/postgres

## Maintained by: [the PostgreSQL Docker Community](https://github.com/docker-library/postgres)

This is the Git repo of the [Docker "Official Image"](https://docs.docker.com/docker-hub/official_repos/) for [postgres](https://hub.docker.com/_/postgres/) (not to be confused with any official postgres image provided by postgres upstream). See [the Docker Hub page](https://hub.docker.com/_/postgres/) for the full readme on how to use this Docker image and for information regarding contributing and issues.

The [full description from Docker Hub](https://hub.docker.com/_/postgres/) is generated over in [docker-library/docs](https://github.com/docker-library/docs), specifically in [docker-library/docs/postgres](https://github.com/docker-library/docs/tree/master/postgres).

## See a change merged here that doesn't show up on Docker Hub yet?

Check [the "library/postgres" manifest file in the docker-library/official-images repo](https://github.com/docker-library/official-images/blob/master/library/postgres), especially [PRs with the "library/postgres" label on that repo](https://github.com/docker-library/official-images/labels/library%2Fpostgres).

For more information about the official images process, see the [docker-library/official-images readme](https://github.com/docker-library/official-images/blob/master/README.md).

---
